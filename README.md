# Spryng SMS API client in PHP

## Installing

The recommended way of installing the spryng client is through composer:

If you don't yet have composer installed:
curl -sS https://getcomposer.org/installer | php

Add spryngclient to your composer.json:
./composer.phar require mvbcoding/spryngclient-php

## Usage

```
#!php

require 'vendor/autoload.php';

$client = new MvBCoding\SpryngClient\Client('username', 'password');

$credits = $client->check_balance();
echo $credits->getMessage();

$result = $client->sendSms('sender', 'destination', 'message');
echo $result->getMessage();

```

## LICENCE
MIT
