<?php

namespace MvBCoding\SpryngClient;

use Psr\Http\Message\ResponseInterface;

class Client
{
    const API_SEND  = 'https://api.spryngsms.com/api/send.php';
    const API_CHECK = 'https://api.spryngsms.com/api/check.php';

    /** @var string */
    private $username;

    /** @var string */
    private $password;

    /**
     * @var string $username
     * @var string $password
     */
    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function sendSms($sender, $destination, $body)
    {
        $allowLong = 0;
        if (strlen($body) > 612) {
            throw new \InvalidArgumentException('Message too big');
        }

        if (strlen($body) > 160) {
            $allowLong = 1;
        }

        $res = $this->doRequest('POST', self::API_SEND, [
            'username'    => $this->username,
            'password'    => $this->password,
            'sender'      => $sender,
            'destination' => $destination,
            'body'        => $body,
            'allowlong'   => $allowLong,
        ]);
        return $res;
    }

    public function check_balance()
    {
        $res = $this->doRequest('GET', self::API_CHECK, ['username' => $this->username, 'password' => $this->password]);
        return $res;
    }

    /**
     * Send data to api and return response
     *
     * @param string $method     HTTP method to use
     * @param string $url        API to send data to
     * @param array  $parameters Data to send to API
     *
     * @return ResponseInterface
     */
    private function doRequest($method, $url, array $parameters = [])
    {
        $method = strtoupper($method);
        $options = [];

        $client = new \GuzzleHttp\Client();
        if (0 < count($parameters)) {
            if ('GET' == $method) {
                $options = ['query' => $parameters];
            }

            if ('POST' == $method) {
                $options = ['form_params' => $parameters];
            }
        }

        $response = $client->request($method, $url, $options);
        return Response::createFromApiResponse($response);
    }
}
