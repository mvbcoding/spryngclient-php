<?php

namespace MvBCoding\SpryngClient;

use Psr\Http\Message\ResponseInterface;

class Response
{
    const SPRYNG_RESPONSE_SUCCESS               = 1;
    const SPRYNG_RESPONSE_MISSING_PARAMETER     = 100;
    const SPRYNG_RESPONSE_USERNAME_TOO_LONG     = 101;
    const SPRYNG_RESPONSE_USERNAME_TOO_SHORT    = 102;
    const SPRYNG_RESPONSE_PASSWORD_TOO_SHORT    = 103;
    const SPRYNG_RESPONSE_PASSWORD_TOO_LONG     = 104;
    const SPRYNG_RESPONSE_DESTINATION_TOO_SHORT = 105;
    const SPRYNG_RESPONSE_DESTINATION_TOO_LONG  = 106;
    const SPRYNG_RESPONSE_SENDER_TOO_LONG       = 107;
    const SPRYNG_RESPONSE_SENDER_TOO_SHORT      = 108;
    const SPRYNG_RESPONSE_CONTENT_TOO_LONG      = 109;
    const SPRYNG_RESPONSE_CONTENT_TOO_SHORT     = 110;
    const SPRYNG_RESPONSE_SECURITY_ERROR        = 200;
    const SPRYNG_RESPONSE_ROUTE_UNKNOWN         = 201;
    const SPRYNG_RESPONSE_ROUTE_VIOLATION       = 202;
    const SPRYNG_RESPONSE_INSUFFICIENT_CREDITS  = 203;
    const SPRYNG_RESPONSE_TECHNICAL_ERROR       = 800;

    const SPRYNG_RESPONSE_STRINGS = [
        self::SPRYNG_RESPONSE_SUCCESS               => 'Succes',
        self::SPRYNG_RESPONSE_MISSING_PARAMETER     => 'Missing parameter',
        self::SPRYNG_RESPONSE_USERNAME_TOO_LONG     => 'Username too long',
        self::SPRYNG_RESPONSE_USERNAME_TOO_SHORT    => 'Username too short',
        self::SPRYNG_RESPONSE_PASSWORD_TOO_SHORT    => 'Password too short',
        self::SPRYNG_RESPONSE_PASSWORD_TOO_LONG     => 'Password too long',
        self::SPRYNG_RESPONSE_DESTINATION_TOO_SHORT => 'Destination too short',
        self::SPRYNG_RESPONSE_DESTINATION_TOO_LONG  => 'Destination too long',
        self::SPRYNG_RESPONSE_SENDER_TOO_LONG       => 'Sender too long',
        self::SPRYNG_RESPONSE_SENDER_TOO_SHORT      => 'Sender too short',
        self::SPRYNG_RESPONSE_CONTENT_TOO_LONG      => 'Content too long',
        self::SPRYNG_RESPONSE_CONTENT_TOO_SHORT     => 'Content too short',
        self::SPRYNG_RESPONSE_SECURITY_ERROR        => 'Security error',
        self::SPRYNG_RESPONSE_ROUTE_UNKNOWN         => 'Route unknown',
        self::SPRYNG_RESPONSE_ROUTE_VIOLATION       => 'Route access violation',
        self::SPRYNG_RESPONSE_INSUFFICIENT_CREDITS  => 'Insufficient credits',
        self::SPRYNG_RESPONSE_TECHNICAL_ERROR       => 'Technical error',
    ];

    /** @var int */
    private $code;

    /** @var string */
    private $message;

    /**
     * Create response object based on API response
     *
     * @param ResponseInterface $response
     *
     * @return $this
     */
    public static function createFromApiResponse(ResponseInterface $response)
    {
        $return = new self();
        $return->setCode(500);
        $return->setMessage('Unknown');

        $code = $response->getStatusCode();
        if (200 === $code) {
            $body = $response->getBody()->getContents();
            if (!array_key_exists($body, self::SPRYNG_RESPONSE_STRINGS)) {
                // This is a response for checkCredits
                $return->setCode(0);
                $return->setMessage(floatval($body));
            } else {
                $return->setCode($body);
                $return->setMessage(self::SPRYNG_RESPONSE_STRINGS[$body]);
            }
        }

        return $return;
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }
}
